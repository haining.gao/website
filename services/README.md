---
title: 事業内容
description: |-
    事業内容
layout: jumbotron
permalink: /services/
js-package: membership
css-package: membership
jumbotron:
    triangle-divider: true
    background-image: /assets/images/content/business-bg.jpg
    title: 事業内容
    description-class: smaller
    description: >-
        事業内容
---
<div class="row padded-row testimonials" id="key-factors">
    <div class="container">
<h2>海天は以下の分野で事業を展開しています。 </h2>
<ul>
<li>システム開発</li> 
<li>クラウドソリューション開発</li>
<li>ソフトウェア受託開発事業</li>
<li>オフショア開発事業</li>
</ul>
システムインテグレーションサービス企業として、企業・個人ユーザーの皆さまにお役に立つサービスを提供していきます。
    </div>
</div>

<div class="row padded-row testimonials" id="key-factors">
    <div class="container">
<h1>システム開発</h1>
当社はこれまで携わった様々な業界の開発経験をベースに、お客様が必要とする情報システムをオーダーメイドで構築できます。複合的なアプリケーション開発から小さなシステム開発まで、 お客様のニーズをきめ細やかに汲み取り、様々なご要望に対応しています。技術者集団による高い「技術力」と創業以来培った「業務ノウハウ」を価値あるサービスの源泉として、お客様にとって最適なパートナーであり続けるために日々努力を重ねています。 <br>
　金融、保険及び製造業などのソフトウェア開発・保守のほか、ビジネスソリューションに至るまで幅広くその技術力をご提供いたします。
<br>
    </div>
</div>

<div class="row padded-row testimonials" id="key-factors">
    <div class="container">
<h1>クラウドソリューション開発</h1>
当社はSalesforce、Amazon Web Services (AWS)などのクラウドサービスを利用し、 お客様それぞれの業務や経営に最適なソリューション提供に参画しております。<br>
<br>
    </div>
</div>


<div class="row padded-row testimonials" id="key-factors">
    <div class="container">
<h1>ソフトウェア受託開発事業</h1>
コンピュータシステムからモバイル関係までシステム開発を受託しております。 中小規模のソフトウェア開発をシステム要件分析、基本設計から本稼働までお客様にご提供いたします。 フェーズ毎に最適な技術者を揃え、品質第一、顧客満足度の向上を努力しております。<br>
    </div>
</div>


<div class="row padded-row testimonials" id="key-factors">
    <div class="container">
<h1>オフショア開発事業</h1>
当社のオフショア開発は、中国のグループ会社にソフトウェアの開発業務を委託しております。 弊社がお客様とオフショア先の間に立つため、お客様はオフショアを意識することなく、 あたかも国内で開発しているかのようなスムーズな指示、要望を出していただくことができます。オフショアのリソースと共に柔軟に対応しております。<br>
    </div>
</div>
